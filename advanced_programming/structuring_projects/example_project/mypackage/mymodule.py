#!/usr/bin/env python


import sys


def myfunction(a, b):
    return a * b


def main():
    if len(sys.argv) != 3:
        print(f'Usage: myscript a b')
        sys.exit(1)

    a = float(sys.argv[1])
    b = float(sys.argv[2])

    print(myfunction(a, b))
