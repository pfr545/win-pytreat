Applications
============

This directory contains a series of practicals which focus
on using Python to accomplish specific tasks, and practicals which introduce
a range of useful Python-based libraries.

Practicals are split into the following sub-categories (and sub-folders):

1. `data_visualisation` : learn to use various data visualisation packages.
2. `fslpy`: a collection of utilities and data abstractions used within FSL and by FSLeyes.
3. `matlab_vs_python` : a number of data analysis examples with head-to-head comparisons between matlab and python code.
4. `modelling` : multiple examples of analysis methods in python.
5. `pandas` : processing and analsing tabular data with the powerful Pandas package.
6. `parallel` : Third-party libraries and strategies for parallelising Python code.
