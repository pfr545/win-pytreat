Getting started
===============


This directory contains the _Getting started_ practicals, intended for those
of you who are new to Python as a language, or want a refresher on how it all
works.


If you are already comfortable with Python, `numpy`, `nibabel`, and `jupyter`.
then you might want to check out the `advanced_programming` or `applications`
folders.


The **Introduction to programming** (`intro_to_programming.ipynb`) practical
is a useful place to start if you have zero or little programming experience.


The **Overview** practical is a whirlwind overview of the Python language,
incorporating bits and pieces from the other practicals. Feel free to start
there, or to start from the **Python basics** practical.


0. Overview (`00_overview.ipynb`) [MC, MJ, PM]
1. Python basics (`01_basics.ipynb`) [MJ]
2. Text input/output(`02_text_io.ipynb`) [MC]
3. File management (`03_file_management.ipynb`) [PM]
4. Numpy (`04_numpy.ipynb`) [PM]
5. Nifti images (`05_nifti.ipynb`) [MJ]
6. Image manipulation (`06_plotting.ipynb`) [MC]
7. Jupyter notebook and IPython (`07_juypter.ipynb`) [MC]
8. Writing a callable script (`08_scripts.ipynb`) [MJ]
